var CFG = {
  scatttering: 40.0,
  fillColor: '#F02375',
  textColor: '#3B2D38',
  sliderColor: '#3B2D38',
  growAnim: 150,
  opacityAnim: (1000 / 5) * 8,
  stepLength: 1000 / 5,
  initR: 2,
  multR: 1.5,
  w: 600,
  h: 420
};

var map;
var overlay;
var step;
var timer = 0;
var paper;
var slider;
var sliderX = 0;
var pointCache = {};
var circles = [];
var date;
var line;
var play;

function createMap () {
  var mapOptions = {
    center: new google.maps.LatLng(55.176643, 23.752441),
    zoom: 7,
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  return map;
}

function getRadius (nJobless) {
  return CFG.initR + Math.sqrt(nJobless) * CFG.multR;
}

function getPixelPoint (x, y) {
  var gMapPoint = new google.maps.LatLng(x, y);

  var pos = overlay.getProjection().fromLatLngToContainerPixel(gMapPoint);
  //var pos = overlay.getProjection().fromLatLngToDivPixel(gMapPoint);
  pos.x = pos.x + (Math.random() - 0.5) * CFG.scatttering;
  pos.y = pos.y + (Math.random() - 0.5) * CFG.scatttering;

  return pos;
};

function next () {
  var points = data[step];
  step++;
  slider.attr({x: (step / data.length) * (CFG.w - 32)});

  if (!points || !points[1]) {
    clearInterval(timer);
    play.style.display = 'block';
    return;
  }

  changeText(points[0]);

  for (var i = 0, p; p = points[1][i]; ++i) {
    var pos = getPixelPoint(p[0], p[1]);
    var radius = p[2];
    var circle = paper.circle(pos.x, pos.y, 1).attr({fill: CFG.fillColor});
    circle.animate({r: CFG.initR + Math.sqrt(p[2]) * CFG.multR}, CFG.growAnim, "<", function () {
      this.animate({opacity: 0.0}, CFG.opacityAnim, ">", function () { this.remove(); });
    });
  }
};

function initialize() {
  map = createMap();
  overlay = new google.maps.OverlayView();
  overlay.draw = function() {};
  overlay.setMap(map);
  paper = new Raphael(document.getElementById("canvas"), CFG.w, CFG.h + 20);

  line = paper.rect(0, CFG.h, CFG.w, 5).attr({'fill': CFG.sliderColor});
  line.click(onLineClick);

  slider = paper.rect(0, CFG.h - 7, 30, 20, 10).attr({'fill': CFG.sliderColor});
  slider.drag(onSliderMove, onSliderStart);
  paper.rect(10, 335, 200, 70, 20).attr({
    fill: '#fff',
    'stroke-opacity': 0.0,
    opacity: 0.7
  });
  date = paper.text(110, 370, '').attr({
    'font-size': 50,
    'stroke': '#fff',
    'stroke-width': 2,
    'stroke-opacity': 0.2,
    'fill': CFG.textColor,
    'font-family': 'Economica'
  });

  step = 0;
  setTimeout(function () {
    timer = setInterval(next, CFG.stepLength);
  }, 500);

  play = document.getElementById('play');
  play.addEventListener('click', function (e) {
    e.preventDefault();
    restart();
  }, false);
}

function changeText (newText) {
  date.attr({text: newText});
}

function onManualStep() {
  var points = data[step];
  changeText(points[0]);

  for (var i = 0, c; c = circles[i]; ++i) {
    c.remove();
  }
  circles = [];

  generateStaticCircles(step, 1.0, 1.0);
  if (step - 1 >= 0)  {
    generateStaticCircles(step - 1, 0.8, 1.0);
  }
  if (step - 2 >= 0)  {
    generateStaticCircles(step - 2, 0.6, 1.0);
  }
  if (step - 3 >= 0)  {
    generateStaticCircles(step - 3, 0.4, 1.0);
  }
  if (step - 4 >= 0)  {
    generateStaticCircles(step - 4, 0.2, 1.0);
  }
  if (step - 5 >= 0)  {
    generateStaticCircles(step - 5, 0.1, 1.0);
  }
}

function generateStaticCircles (lStep, alpha, radiusMultiply) {
  var points = data[lStep];
  for (var i = 0, p; p = points[1][i]; ++i) {
    var key = '' + lStep + ':' + i;
    var pos;

    if (pointCache[key]) {
      pos = pointCache[key];
    } else {
      pos = getPixelPoint(p[0], p[1]);
      pointCache[key] = pos;
    }

    var circle = paper.circle(pos.x, pos.y, getRadius(p[2])).attr({
      fill: CFG.fillColor,
      opacity: alpha
    });
    circles.push(circle);
  }
}

function onSliderStart() {
  clearInterval(timer);
  play.style.display = 'block';

  sliderX = slider.attr('x');
}

function onLineClick (e) {
  clearInterval(timer);
  play.style.display = 'block';

  sliderX = e.layerX - 15;
  onSliderMove(0);
}

function onSliderMove(dx) {
  var newX = sliderX + dx;
  step = parseInt((newX / (CFG.w - 30)) * data.length, 10);
  if (newX >= CFG.w - 30) {
    newX = CFG.w - 30;
    step = data.length - 1;
  }
  if (newX <= 0) {
    step = 0;
    newX = 0;
  }
  slider.attr({x: newX});
  onManualStep();
}

function restart () {
  play.style.display = 'none';

  step = 0;
  for (var i = 0, c; c = circles[i]; ++i) {
    c.remove();
  }
  timer = setInterval(next, CFG.stepLength);
}
